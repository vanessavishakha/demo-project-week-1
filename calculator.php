<?php
$err = array("emp"=>"Enter values","non-numeric"=>"Enter numeric values","/by0"=>"Cannot be divisible by 0");
if(isset($_POST['num1']) and isset($_POST['num2']))
{
    $n1 = $_POST['num1'];
    $n2 = $_POST['num2'];
}
$res = "";
if(isset($n1) and isset($n2))
{
    if($n1 != "" or $n2 != "")
    {
        if(is_numeric($n1) and is_numeric($n2))
        {
            if(isset($_POST['add']))
            {
                $res = $n1+$n2;
            }
            if(isset($_POST['sub']))
            {
                $res = $n1-$n2;
            }
            if(isset($_POST['mul']))
            {
                $res = $n1*$n2;
            }
            if(isset($_POST['div']))
            {
                if($n2 == 0)
                    echo $err['/by0'];
                else
                {
                    $res = $n1/$n2;
                }
            }
        }
        else
        {
            echo $err['non-numeric'];
        }
    }
}
else
{
    echo $err['emp'];
}
?>
<html>
<head>
<title>Calculator</title>
<style type="text/css">
    input[type=submit]{
        background-color: lightblue;
        color: white;
        padding: 16px 32px;
        margin: 4px 2px;
    }
    input[type=text]{
        background-color: lightpink;
        color: black;
        padding: 16px 32px;
        margin: 4px 2px;
    }
    form{
        background-color: lightyellow; 
        text-align:center;
    }
</style>
</head>
<body>
<form method="post" action="<?=$_SERVER["PHP_SELF"];?>">

Input no 1:<input width:100% type="text" name="num1""></br>
</br>
Input no 2:<input type="text" name="num2"></br>
</br>
<input type="submit" value="+" name="add" >
<input type="submit" value="-" name="sub" ></br>
<input type="submit" value="*" name="mul" >
<input type="submit" value="/" name="div" ></br>
Answer:<input type="text" value="<?php echo $res;?>" name="ans"></br>
</form>
</body>
</html>
