<?php

require_once "src/Calculator.php";

$result = "";
if (isset($_POST['inputField1']) AND isset($_POST['inputField2'])) {

    $number1 = trim($_POST['inputField1']);
    $number2 = trim($_POST['inputField2']);
    $result = "";
    if (isset($number1) AND isset($number2)) {
        if ($number1 != "" OR $number2 != "") {
            if (is_numeric($number1) AND is_numeric($number2)) {
                $calculatorObject = new Calculator($number1, $number2);
                if (isset($_POST['add'])) {
                    $result = $calculatorObject->addition();
                }
                if (isset($_POST['sub'])) {
                    $result = $calculatorObject->subtraction();
                }
                if (isset($_POST['mul'])) {
                    $result = $calculatorObject->multiplication();
                }
                if (isset($_POST['div'])) {
                    $result = $calculatorObject->division();
                }
            } else {
                echo "Enter numeric values";
            }
        } else {
            echo "Enter values field is empty";
        }
    }
}
?>
<html>
<head>
    <title>Calculator</title>
    <style type="text/css">
        input[type=submit] {
            background-color: lightblue;
            color: white;
            padding: 16px 32px;
            margin: 4px 2px;
        }

        input[type=text] {
            background-color: lightpink;
            color: black;
            padding: 16px 32px;
            margin: 4px 2px;
        }

        form {
            background-color: lightyellow;
            text-align: center;
        }
    </style>
</head>
<body>
<form method="POST" action="<?= $_SERVER["PHP_SELF"]; ?>">

    Input no 1:<input type="text" name="inputField1""></br>
    </br>
    Input no 2:<input type="text" name="inputField2"></br>
    </br>
    <input type="submit" value="+" name="add">
    <input type="submit" value="-" name="sub"></br>
    <input type="submit" value="*" name="mul">
    <input type="submit" value="/" name="div"></br>
    Answer:<input type="text" value="<?php echo $result; ?>" name="ans"></br>
</form>
</body>
</html>
