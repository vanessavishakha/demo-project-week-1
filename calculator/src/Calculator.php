<?php

class Calculator
{
    public $numberOne;
    public $numberTwo;

    public function __construct($number1, $number2)
    {
        $this->numberOne = $number1;
        $this->numberTwo = $number2;
    }

    public function addition()
    {
        return ($this->numberOne + $this->numberTwo);
    }

    public function subtraction()
    {
        return ($this->numberOne - $this->numberTwo);
    }

    public function multiplication()
    {
        return ($this->numberOne * $this->numberTwo);
    }

    public function division()
    {
        if ($this->numberTwo == 0) {
            return "Cannot be divisible by 0";
        } else {
            return ($this->numberOne / $this->numberTwo);
        }
    }
}